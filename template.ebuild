# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-1 luajit )
PYTHON_COMPAT=( python3_{9..11} )

inherit lua-single python-single-r1 xdg

DESCRIPTION=""
HOMEPAGE=""

if [[ ${PV} == *9999* ]]; then
   inherit git-r3
   EGIT_REPO_URI=""
else
   KEYWORDS="~amd64"
   SRC_URI=""
fi

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
