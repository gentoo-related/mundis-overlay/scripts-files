URL="https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main"



cat > README.md << "EOF"
Adding the overlay
------------------
##### Using app-eselect/eselect-repository
eselect repository add mundis git https://gitlab.com/gentoo-related/mundis-overlay/mundis.git  
emerge --sync  
##### If you really wanna use app-portage/layman
uncomment in: `/etc/layman/layman.cfg` the line  
`# overlay_defs : /etc/layman/overlays` to  
`overlay_defs : /etc/layman/overlays`  
and use the following commands  
`wget -P /etc/layman/overlays/ https://gitlab.com/gentoo-related/mundis-overlay/scripts-files/-/raw/main/mundis.xml`  
`layman -L`  
`layman -a mundis`  

##### Ebuilds Overview:

EOF
dirname `find -name *.ebuild | sed 's/\.\///g'` | uniq > ../ebuild_list.txt
count=1;while read line; do EBUILD[count]=$line;count=$[count+1]; done < ../ebuild_list.txt
grep -h DESCRIPTION= `find -name *.ebuild` | sed 's/DESCRIPTION=//g' | sed 's/"//g' | uniq > ../description_list.txt
count=1;while read line; do DESCRIPTION[count]=$line;count=$[count+1]; done < ../description_list.txt



echo "<table>" >> README.md
for i in $(seq 1 ${#EBUILD[*]});do
	echo "<tr><td>" >> README.md
	echo "<a href=${URL}/${EBUILD[i]}>${EBUILD[i]%%.ebuild}</a>" >> README.md
	echo "</td><td>" >> README.md
	echo "${DESCRIPTION[i]}" >> README.md
	echo "</td></tr>" >> README.md
done
echo "</table>" >> README.md

git add *
git commit -a
git push
